package com.example.hanoitravel.SplashOnboardActivity;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.hanoitravel.R;
import com.example.hanoitravel.SigninActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class OnboardingFragmentThird extends Fragment {
    private Button btnStart;
    private View mView;

    public OnboardingFragmentThird() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_onboarding_third, container, false);

        btnStart = mView.findViewById(R.id.btnStart);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SigninActivity.class);
                getActivity().startActivity(intent);
            }
        });
        return mView;
    }
}