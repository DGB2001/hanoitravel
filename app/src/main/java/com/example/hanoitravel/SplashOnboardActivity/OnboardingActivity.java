package com.example.hanoitravel.SplashOnboardActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.example.hanoitravel.R;

import me.relex.circleindicator.CircleIndicator3;

public class OnboardingActivity extends AppCompatActivity {
    private Button btnSkip, btnContinue;
    private ViewPager2 viewPager;
    private RelativeLayout layoutBottom;
    private CircleIndicator3 circleIndicator;

    private ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);

        getView();
        setEventOnClick();

        viewPagerAdapter = new ViewPagerAdapter(this);
        viewPager.setAdapter(viewPagerAdapter);
        circleIndicator.setViewPager(viewPager);


        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                if (position == 2) {
                    btnSkip.setVisibility(View.GONE);
                    btnContinue.setVisibility(View.GONE);
                    layoutBottom.setVisibility(View.GONE);
                }
                else {
                    btnSkip.setVisibility(View.VISIBLE);
                    btnContinue.setVisibility(View.VISIBLE);
                    layoutBottom.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void getView() {
        btnSkip = findViewById(R.id.btnSkip);
        btnContinue = findViewById(R.id.btnContinue);
        viewPager = findViewById(R.id.view_pager_onboarding);
        layoutBottom = findViewById(R.id.layoutBottom);
        circleIndicator = findViewById(R.id.circle_indicator);
    }

    private void setEventOnClick() {
        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(2);
            }
        });
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewPager.getCurrentItem() < 2) {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() +1);
                }
            }
        });
    }
}