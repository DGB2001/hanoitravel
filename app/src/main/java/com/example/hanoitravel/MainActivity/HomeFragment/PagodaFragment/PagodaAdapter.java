package com.example.hanoitravel.MainActivity.HomeFragment.PagodaFragment;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hanoitravel.MainActivity.DetailLocationActivity;
import com.example.hanoitravel.MainActivity.FavouriteFragment.FavouriteAdapter;
import com.example.hanoitravel.MainActivity.MainActivity;
import com.example.hanoitravel.R;

import java.util.List;

public class PagodaAdapter extends RecyclerView.Adapter<PagodaAdapter.PagodaViewHolder> {
    private PagodaFragment mContext;
    private List<Pagoda> mListPagoda;

    public PagodaAdapter(PagodaFragment mContext) {
        this.mContext = mContext;
    }

    public void setData(List<Pagoda> list) {
        this.mListPagoda = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PagodaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_location, parent, false);
        return new PagodaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PagodaViewHolder holder, int position) {
        Pagoda pagoda = mListPagoda.get(position);
        if (pagoda == null) {
            return;
        }
        holder.imgLocation.setImageResource(pagoda.getResourceImg());
        holder.tvLocation.setText(pagoda.getName());

        holder.cvLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), DetailLocationActivity.class);

                // Send data
                intent.putExtra("Image", mListPagoda.get(holder.getAbsoluteAdapterPosition()).getResourceImg());
                intent.putExtra("Name", mListPagoda.get(holder.getAbsoluteAdapterPosition()).getName());
                intent.putExtra("Location", mListPagoda.get(holder.getAbsoluteAdapterPosition()).getLocation());
                intent.putExtra("Description", mListPagoda.get(holder.getAbsoluteAdapterPosition()).getDescription());
                mContext.startActivity(intent);
            }
        });

        holder.btnFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pagoda.getFavStatus().equals("0")) {
                    pagoda.setFavStatus("1");
                    holder.btnFavourite.setImageResource(R.drawable.ic_favorite_red);
                    // Send data
                    Intent intent = new Intent(v.getContext(), MainActivity.class);
                    intent.putExtra("Image", mListPagoda.get(holder.getAbsoluteAdapterPosition()).getResourceImg());
                    intent.putExtra("Name", mListPagoda.get(holder.getAbsoluteAdapterPosition()).getName());
                } else {
                    pagoda.setFavStatus("0");
                    holder.btnFavourite.setImageResource(R.drawable.ic_favorite_shadow);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mListPagoda != null) {
            return mListPagoda.size();
        }
        return 0;
    }

    public class PagodaViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgLocation;
        private TextView tvLocation;
        private CardView cvLocation;
        private ImageView btnFavourite;

        public PagodaViewHolder(@NonNull View itemView) {
            super(itemView);

            imgLocation = itemView.findViewById(R.id.imgLocation);
            tvLocation = itemView.findViewById(R.id.tvLocation);
            cvLocation = itemView.findViewById(R.id.cvLocation);
            btnFavourite = itemView.findViewById(R.id.btnFavourite);

//            btnFavourite.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    int position = getAbsoluteAdapterPosition();
//                    Pagoda pagoda = mListPagoda.get(position);
//
//                    if (pagoda.getFavStatus().equals("0")) {
//                        pagoda.setFavStatus("1");
//                        btnFavourite.setImageResource(R.drawable.ic_favorite_red);
//                    } else {
//                        pagoda.setFavStatus("0");
//                        btnFavourite.setImageResource(R.drawable.ic_favorite_shadow);
//                    }
//                }
//            });
        }
    }
}
