package com.example.hanoitravel.MainActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.hanoitravel.MainActivity.FavouriteFragment.FavoriteFragment;
import com.example.hanoitravel.MainActivity.HomeFragment.PagodaFragment.PagodaFragment;
import com.example.hanoitravel.PageTransformer.DepthPageTransformer;
import com.example.hanoitravel.PageTransformer.ZoomOutPageTransformer;
import com.example.hanoitravel.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

public class MainActivity extends AppCompatActivity {
    private ViewPager2 viewPager2;
    private BottomNavigationView bottomNavigationView;
    private MainViewPagerAdapter mainViewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager2 = findViewById(R.id.view_pager_main);
        bottomNavigationView = findViewById(R.id.bottom_navigation);

        mainViewPagerAdapter = new MainViewPagerAdapter(this);
        viewPager2.setAdapter(mainViewPagerAdapter);

        bottomNavigationView.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                viewPager2.setPageTransformer(new ZoomOutPageTransformer());
                switch (id) {
                    case R.id.menu_home: viewPager2.setCurrentItem(0); break;
                    case R.id.menu_map: viewPager2.setCurrentItem(1); break;
                    case R.id.menu_favorite: viewPager2.setCurrentItem(2); break;
                    case R.id.menu_person: viewPager2.setCurrentItem(3); break;
                }
                return true;
            }
        });

        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                viewPager2.setPageTransformer(new DepthPageTransformer());
                switch (position) {
                    case 0:
                        bottomNavigationView.getMenu().findItem(R.id.menu_home).setChecked(true);
                        break;
                    case 1:
                        bottomNavigationView.getMenu().findItem(R.id.menu_map).setChecked(true);
                        break;
                    case 2:
                        bottomNavigationView.getMenu().findItem(R.id.menu_favorite).setChecked(true);
                        break;
                    case 3:
                        bottomNavigationView.getMenu().findItem(R.id.menu_person).setChecked(true);
                        break;
                }
            }
        });

        // Receive data
//        Intent intent = getIntent();
//        int image = intent.getExtras().getInt("Image");
//        String Name = intent.getExtras().getString("Name");

        // Send data
//        Bundle bundle = new Bundle();
//        bundle.putInt("Image", image);
//        bundle.putString("Name", Name);
//
//        FavoriteFragment fragmentActivity = new FavoriteFragment();
//        fragmentActivity.setArguments(bundle);
    }
}