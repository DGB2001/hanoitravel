package com.example.hanoitravel.MainActivity.HomeFragment.ParkFragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hanoitravel.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ParkFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ParkFragment extends Fragment {
    private View mView;

    private RecyclerView rcvPark;
    private ParkAdapter mParkAdapter;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ParkFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ParkFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ParkFragment newInstance(String param1, String param2) {
        ParkFragment fragment = new ParkFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_park, container, false);
        // TO DO
        rcvPark= mView.findViewById(R.id.rcvPark);
        mParkAdapter = new ParkAdapter(this);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),2);
        rcvPark.setLayoutManager(gridLayoutManager);

        mParkAdapter.setData(getListPark());
        rcvPark.setAdapter(mParkAdapter);
        return mView;
    }

    private List<Park> getListPark() {
        List<Park> list = new ArrayList<>();
        list.add(new Park(R.drawable.cong_vien_thong_nhat, "Công viên thống nhất 1","Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 1","0"));
        list.add(new Park(R.drawable.cong_vien_thong_nhat, "Công viên thống nhất 2","Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 2","0"));
        list.add(new Park(R.drawable.cong_vien_thong_nhat, "Công viên thống nhất 3","Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 3","0"));
        list.add(new Park(R.drawable.cong_vien_thong_nhat, "Công viên thống nhất 4","Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 4","0"));
        list.add(new Park(R.drawable.cong_vien_thong_nhat, "Công viên thống nhất 5","Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 5","0"));
        list.add(new Park(R.drawable.cong_vien_thong_nhat, "Công viên thống nhất 6","Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 6","0"));
        list.add(new Park(R.drawable.cong_vien_thong_nhat, "Công viên thống nhất 7","Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 7","0"));
        list.add(new Park(R.drawable.cong_vien_thong_nhat, "Công viên thống nhất 8","Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 8","0"));

        return list;
    }
}