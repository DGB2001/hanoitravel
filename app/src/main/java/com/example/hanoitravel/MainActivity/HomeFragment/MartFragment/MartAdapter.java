package com.example.hanoitravel.MainActivity.HomeFragment.MartFragment;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hanoitravel.MainActivity.DetailLocationActivity;
import com.example.hanoitravel.MainActivity.HomeFragment.PagodaFragment.Pagoda;
import com.example.hanoitravel.R;

import java.util.List;

public class MartAdapter extends RecyclerView.Adapter<MartAdapter.MartViewHolder> {
    private MartFragment mContext;
    private List<Mart> mListMart;

    public MartAdapter(MartFragment mContext) {
        this.mContext = mContext;
    }

    public void setData(List<Mart> list) {
        this.mListMart = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MartAdapter.MartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_location,parent,false);
        return new MartAdapter.MartViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MartAdapter.MartViewHolder holder, int position) {
        Mart mart = mListMart.get(position);
        if (mart==null) {
            return;
        }
        holder.imgLocation.setImageResource(mart.getResourceImg());
        holder.tvLocation.setText(mart.getName());

        holder.cvLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), DetailLocationActivity.class);

                // Send data
                intent.putExtra("Image",mListMart.get(holder.getAbsoluteAdapterPosition()).getResourceImg());
                intent.putExtra("Name",mListMart.get(holder.getAbsoluteAdapterPosition()).getName());
                intent.putExtra("Location",mListMart.get(holder.getAbsoluteAdapterPosition()).getLocation());
                intent.putExtra("Description",mListMart.get(holder.getAbsoluteAdapterPosition()).getDescription());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mListMart!=null) {
            return mListMart.size();
        }
        return 0;
    }

    public class MartViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgLocation;
        private TextView tvLocation;
        private CardView cvLocation;
        private ImageView btnFavourite;

        public MartViewHolder(@NonNull View itemView) {
            super(itemView);

            imgLocation = itemView.findViewById(R.id.imgLocation);
            tvLocation = itemView.findViewById(R.id.tvLocation);
            cvLocation = itemView.findViewById(R.id.cvLocation);
            btnFavourite = itemView.findViewById(R.id.btnFavourite);

            btnFavourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAbsoluteAdapterPosition();
                    Mart mart = mListMart.get(position);

                    if (mart.getFavStatus().equals("0")) {
                        mart.setFavStatus("1");
                        btnFavourite.setImageResource(R.drawable.ic_favorite_red);
                    } else {
                        mart.setFavStatus("0");
                        btnFavourite.setImageResource(R.drawable.ic_favorite_shadow);
                    }
                }
            });
        }
    }
}
