package com.example.hanoitravel.MainActivity.HomeFragment.MartFragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hanoitravel.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MartFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MartFragment extends Fragment {
    private View mView;

    private RecyclerView rcvMart;
    private MartAdapter mMartAdapter;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public MartFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MartFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MartFragment newInstance(String param1, String param2) {
        MartFragment fragment = new MartFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_mart, container, false);
        // TO DO
        rcvMart = mView.findViewById(R.id.rcvMart);
        mMartAdapter = new MartAdapter(this);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),2);
        rcvMart.setLayoutManager(gridLayoutManager);

        mMartAdapter.setData(getListMart());
        rcvMart.setAdapter(mMartAdapter);
        return mView;
    }

    private List<Mart> getListMart() {
        List<Mart> list = new ArrayList<>();
        list.add(new Mart(R.drawable.tttm_aeon, "AEON MALL Long Biên 1","Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 1","0"));
        list.add(new Mart(R.drawable.tttm_aeon, "AEON MALL Long Biên 2","Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 2","0"));
        list.add(new Mart(R.drawable.tttm_aeon, "AEON MALL Long Biên 3","Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 3","0"));
        list.add(new Mart(R.drawable.tttm_aeon, "AEON MALL Long Biên 4","Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 4","0"));
        list.add(new Mart(R.drawable.tttm_aeon, "AEON MALL Long Biên 5","Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 5","0"));
        list.add(new Mart(R.drawable.tttm_aeon, "AEON MALL Long Biên 6","Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 6","0"));
        list.add(new Mart(R.drawable.tttm_aeon, "AEON MALL Long Biên 7","Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 7","0"));
        list.add(new Mart(R.drawable.tttm_aeon, "AEON MALL Long Biên 8","Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 8","0"));

        return list;
    }
}