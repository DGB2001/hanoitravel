package com.example.hanoitravel.MainActivity;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.hanoitravel.MainActivity.FavouriteFragment.FavoriteFragment;
import com.example.hanoitravel.MainActivity.HomeFragment.HomeFragment;

public class MainViewPagerAdapter extends FragmentStateAdapter {

    public MainViewPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0: return new HomeFragment();
            case 1: return new DiscoveryFragment();
            case 2: return new FavoriteFragment();
            case 3: return new PersonFragment();
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return 4;
    }
}
