package com.example.hanoitravel.MainActivity.HomeFragment.MuseumFragment;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hanoitravel.MainActivity.DetailLocationActivity;
import com.example.hanoitravel.MainActivity.HomeFragment.PagodaFragment.Pagoda;
import com.example.hanoitravel.R;

import java.util.List;

public class MuseumAdapter extends RecyclerView.Adapter<MuseumAdapter.MuseumViewHolder>{
    private MuseumFragment mContext;
    private List<Museum> mListMuseum;

    public MuseumAdapter(MuseumFragment mContext) {
        this.mContext = mContext;
    }

    public void setData(List<Museum> list) {
        this.mListMuseum = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MuseumAdapter.MuseumViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_location,parent,false);
        return new MuseumAdapter.MuseumViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MuseumAdapter.MuseumViewHolder holder, int position) {
        Museum museum = mListMuseum.get(position);
        if (museum==null) {
            return;
        }
        holder.imgLocation.setImageResource(museum.getResourceImg());
        holder.tvLocation.setText(museum.getName());

        holder.cvLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), DetailLocationActivity.class);

                // Send data
                intent.putExtra("Image",mListMuseum.get(holder.getAbsoluteAdapterPosition()).getResourceImg());
                intent.putExtra("Name",mListMuseum.get(holder.getAbsoluteAdapterPosition()).getName());
                intent.putExtra("Location",mListMuseum.get(holder.getAbsoluteAdapterPosition()).getLocation());
                intent.putExtra("Description",mListMuseum.get(holder.getAbsoluteAdapterPosition()).getDescription());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mListMuseum!=null) {
            return mListMuseum.size();
        }
        return 0;
    }

    public class MuseumViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgLocation;
        private TextView tvLocation;
        private CardView cvLocation;
        private ImageView btnFavourite;

        public MuseumViewHolder(@NonNull View itemView) {
            super(itemView);

            imgLocation = itemView.findViewById(R.id.imgLocation);
            tvLocation = itemView.findViewById(R.id.tvLocation);
            cvLocation = itemView.findViewById(R.id.cvLocation);
            btnFavourite = itemView.findViewById(R.id.btnFavourite);

            btnFavourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAbsoluteAdapterPosition();
                    Museum museum = mListMuseum.get(position);

                    if (museum.getFavStatus().equals("0")) {
                        museum.setFavStatus("1");
                        btnFavourite.setImageResource(R.drawable.ic_favorite_red);
                    } else {
                        museum.setFavStatus("0");
                        btnFavourite.setImageResource(R.drawable.ic_favorite_shadow);
                    }
                }
            });
        }
    }
}
