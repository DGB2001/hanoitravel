package com.example.hanoitravel.MainActivity.HomeFragment.PagodaFragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.hanoitravel.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PagodaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PagodaFragment extends Fragment {
    private View mView;

    private RecyclerView rcvPagoda;
    private PagodaAdapter mPagodaAdapter;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public PagodaFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PagodaFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PagodaFragment newInstance(String param1, String param2) {
        PagodaFragment fragment = new PagodaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_pagoda, container, false);
        // TO DO
        rcvPagoda = mView.findViewById(R.id.rcvPagoda);
        mPagodaAdapter = new PagodaAdapter(this);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),2);
        rcvPagoda.setLayoutManager(gridLayoutManager);

        mPagodaAdapter.setData(getListPagoda());
        rcvPagoda.setAdapter(mPagodaAdapter);
        return mView;
    }

    private List<Pagoda> getListPagoda() {
        List<Pagoda> list = new ArrayList<>();
        list.add(new Pagoda(R.drawable.chua_mot_cot, "Chùa Một Cột 1", "Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 1","0"));
        list.add(new Pagoda(R.drawable.chua_mot_cot, "Chùa Một Cột 2", "Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 2","0"));
        list.add(new Pagoda(R.drawable.chua_mot_cot, "Chùa Một Cột 3", "Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 3","0"));
        list.add(new Pagoda(R.drawable.chua_mot_cot, "Chùa Một Cột 4", "Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 4","0"));
        list.add(new Pagoda(R.drawable.chua_mot_cot, "Chùa Một Cột 1", "Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 5","0"));
        list.add(new Pagoda(R.drawable.chua_mot_cot, "Chùa Một Cột 2", "Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 6","0"));
        list.add(new Pagoda(R.drawable.chua_mot_cot, "Chùa Một Cột 3", "Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 7","0"));
        list.add(new Pagoda(R.drawable.chua_mot_cot, "Chùa Một Cột 4", "Địa chỉ: 2RPM+8C Ba Đình, Hà Nội, Việt Nam", "Đây là phần mô tả địa điểm 8","0"));

        return list;
    }
}