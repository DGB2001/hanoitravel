package com.example.hanoitravel.MainActivity.HomeFragment;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.hanoitravel.MainActivity.HomeFragment.MartFragment.MartFragment;
import com.example.hanoitravel.MainActivity.HomeFragment.MuseumFragment.MuseumFragment;
import com.example.hanoitravel.MainActivity.HomeFragment.PagodaFragment.PagodaFragment;
import com.example.hanoitravel.MainActivity.HomeFragment.ParkFragment.ParkFragment;

public class HomeFragmentStateAdapter extends FragmentStateAdapter {

    public HomeFragmentStateAdapter(@NonNull HomeFragment fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0: return new PagodaFragment();
            case 1: return new MuseumFragment();
            case 2: return new ParkFragment();
            case 3: return new MartFragment();
            default: return null;
        }
    }

    @Override
    public int getItemCount() {
        return 4;
    }
}
