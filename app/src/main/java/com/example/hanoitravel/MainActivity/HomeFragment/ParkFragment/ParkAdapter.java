package com.example.hanoitravel.MainActivity.HomeFragment.ParkFragment;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hanoitravel.MainActivity.DetailLocationActivity;
import com.example.hanoitravel.MainActivity.HomeFragment.PagodaFragment.Pagoda;
import com.example.hanoitravel.R;

import java.util.List;

public class ParkAdapter extends RecyclerView.Adapter<ParkAdapter.ParkViewHolder> {
    private ParkFragment mContext;
    private List<Park> mListPark;

    public ParkAdapter(ParkFragment mContext) {
        this.mContext = mContext;
    }

    public void setData(List<Park> list) {
        this.mListPark = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ParkAdapter.ParkViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_location,parent,false);
        return new ParkAdapter.ParkViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ParkAdapter.ParkViewHolder holder, int position) {
        Park park = mListPark.get(position);
        if (park==null) {
            return;
        }
        holder.imgLocation.setImageResource(park.getResourceImg());
        holder.tvLocation.setText(park.getName());

        holder.cvLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), DetailLocationActivity.class);

                // Send data
                intent.putExtra("Image",mListPark.get(holder.getAbsoluteAdapterPosition()).getResourceImg());
                intent.putExtra("Name",mListPark.get(holder.getAbsoluteAdapterPosition()).getName());
                intent.putExtra("Location",mListPark.get(holder.getAbsoluteAdapterPosition()).getLocation());
                intent.putExtra("Description",mListPark.get(holder.getAbsoluteAdapterPosition()).getDescription());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mListPark!=null) {
            return mListPark.size();
        }
        return 0;
    }

    public class ParkViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgLocation;
        private TextView tvLocation;
        private CardView cvLocation;
        private ImageView btnFavourite;

        public ParkViewHolder(@NonNull View itemView) {
            super(itemView);

            imgLocation = itemView.findViewById(R.id.imgLocation);
            tvLocation = itemView.findViewById(R.id.tvLocation);
            cvLocation = itemView.findViewById(R.id.cvLocation);
            btnFavourite = itemView.findViewById(R.id.btnFavourite);

            btnFavourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAbsoluteAdapterPosition();
                    Park park = mListPark.get(position);

                    if (park.getFavStatus().equals("0")) {
                        park.setFavStatus("1");
                        btnFavourite.setImageResource(R.drawable.ic_favorite_red);
                    } else {
                        park.setFavStatus("0");
                        btnFavourite.setImageResource(R.drawable.ic_favorite_shadow);
                    }
                }
            });
        }
    }
}
