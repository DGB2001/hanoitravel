package com.example.hanoitravel.MainActivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hanoitravel.R;

public class DetailLocationActivity extends AppCompatActivity {
    private ImageView imgLocation;
    private TextView tvName, tvLocation, tvDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_location);

        imgLocation = findViewById(R.id.imgLocation);
        tvName = findViewById(R.id.tvName);
        tvLocation = findViewById(R.id.tvLocation);
        tvDescription = findViewById(R.id.tvDescription);

        // Receive data
        Intent intent = getIntent();
        int image = intent.getExtras().getInt("Image");
        String Name = intent.getExtras().getString("Name");
        String Location = intent.getExtras().getString("Location");
        String Description = intent.getExtras().getString("Description");

        // Set value
        imgLocation.setImageResource(image);
        tvName.setText(Name);
        tvLocation.setText(Location);
        tvDescription.setText(Description);
    }
}