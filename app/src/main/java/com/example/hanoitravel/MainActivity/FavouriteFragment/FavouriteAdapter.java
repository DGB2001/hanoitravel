package com.example.hanoitravel.MainActivity.FavouriteFragment;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hanoitravel.MainActivity.DetailLocationActivity;
import com.example.hanoitravel.MainActivity.HomeFragment.PagodaFragment.Pagoda;
import com.example.hanoitravel.R;

import java.util.List;

public class FavouriteAdapter extends RecyclerView.Adapter<FavouriteAdapter.FavouriteViewHolder> {
    private FavoriteFragment mContext;
    private List<FavouriteLocation> mListFavouriteLocation;

    public FavouriteAdapter(FavoriteFragment mContext) {
        this.mContext = mContext;
    }

    public void setData(List<FavouriteLocation> list) {
        this.mListFavouriteLocation = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public FavouriteAdapter.FavouriteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favourite_location,parent,false);
        return new FavouriteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavouriteAdapter.FavouriteViewHolder holder, int position) {
        FavouriteLocation favouriteLocation = mListFavouriteLocation.get(position);
        if (favouriteLocation==null) {
            return;
        }

        // Receive data
//        Intent intent = getIntent();
//        int image = intent.getExtras().getInt("Image");
//        String Name = intent.getExtras().getString("Name");

        holder.imgLocation.setImageResource(favouriteLocation.getResourceImg());
        holder.tvLocation.setText(favouriteLocation.getName());

        holder.cvLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), DetailLocationActivity.class);

                // Send data
                intent.putExtra("Image", mListFavouriteLocation.get(holder.getAbsoluteAdapterPosition()).getResourceImg());
                intent.putExtra("Name", mListFavouriteLocation.get(holder.getAbsoluteAdapterPosition()).getName());
                intent.putExtra("Location", mListFavouriteLocation.get(holder.getAbsoluteAdapterPosition()).getLocation());
                intent.putExtra("Description", mListFavouriteLocation.get(holder.getAbsoluteAdapterPosition()).getDescription());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mListFavouriteLocation != null) {
            return mListFavouriteLocation.size();
        }
        return 0;
    }

    public class FavouriteViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgLocation;
        private TextView tvLocation;
        private CardView cvLocation;
        private ImageView btnFavourite;

        public FavouriteViewHolder(@NonNull View itemView) {
            super(itemView);

            imgLocation = itemView.findViewById(R.id.imgLocation);
            tvLocation = itemView.findViewById(R.id.tvLocation);
            cvLocation = itemView.findViewById(R.id.cvLocation);
            btnFavourite = itemView.findViewById(R.id.btnFavourite);

            // Remove from favourite list after click
//            btnFavourite.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    int position = getAbsoluteAdapterPosition();
//                    FavouriteLocation favouriteLocation = mListFavouriteLocation.get(position);
//                    mListFavouriteLocation.remove(position);
//                    notifyItemRemoved(position);
//                }
//            });
        }
    }
}
