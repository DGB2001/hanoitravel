package com.example.hanoitravel.MainActivity.HomeFragment.PagodaFragment;

import android.net.LinkAddress;

import com.google.android.gms.maps.MapView;

public class Pagoda {
    private int resourceImg;
    private String name;
    private String location;
    private String description;
    private String favStatus;

    public Pagoda(int resourceImg, String name, String location, String description, String favStatus) {
        this.resourceImg = resourceImg;
        this.name = name;
        this.location = location;
        this.description = description;
        this.favStatus = favStatus;
    }

    public int getResourceImg() {
        return resourceImg;
    }

    public void setResourceImg(int resourceImg) {
        this.resourceImg = resourceImg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFavStatus() {
        return favStatus;
    }

    public void setFavStatus(String favStatus) {
        this.favStatus = favStatus;
    }
}
